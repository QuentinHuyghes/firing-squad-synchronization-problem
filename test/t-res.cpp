#include <iostream>
#include <fstream>
#include <string>

#include <base/solution.h>
#include <base/automata.h>

int main(int argc, char ** argv) {
	// number of states
	unsigned nbStates = 5;

	int tab[] = {2,3,4,5,7,10};
	char name[256];
	for(int i=1000; i<=1000000; i=i*10){
		for(int j=10; j<=1000; j=j*10){
			for(int k=0; k<6; k++){
				sprintf(name, "%d_iter_%d_ils_%d_perturbations.csv", i, j, tab[k]);
				std::cout << name << std::endl;
				Solution x(nbStates);
				Automata a(30);
				a.iteratedLocalSearch(x, 30, i, j, k, name);
			}
		}
	}
}
